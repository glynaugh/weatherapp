package main

import (
	"testing"
)

func TestGetAPIURL(t *testing.T){
	result := getAPIURL("25,-100")
	if result != "https://api.darksky.net/forecast/026d59d2af0e72969f9968eaf88b6354/25,-100" {
		t.Error("expected different URL")
	}
}
