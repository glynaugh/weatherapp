function getLocationFromPage(){
    let location = document.getElementById('location').value;
    return location
}

function checkIfValidGPSCoords(location){
    try {
        let coords = location.split(',');
        let latitude = Number(coords[0]);
        let longitude = Number(coords[1]);
        if (latitude > 90 || latitude < -90 || isNaN(latitude)){
            return false
        }
        if (longitude > 180 || longitude < -180 || isNaN(longitude)){
            return false
        }
        return true
    }
    catch (err){
        return false
    }
}

function getWeatherInfo(location){
    let data = { "location": location };
    return new Promise ((resolve, reject) => {
        fetch( '/submit', {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json'
            }
        }).then( (result) => {
            result.json()
            .then( (response) => {
                resolve(response);
            })
        })
    })
}

function getDailyDataFromJSON(jsonResponse){
    return jsonResponse.daily.data
}

function getDayOfWeekFromJSON(dayData){
    dateInUnix = dayData.time; 
    dayOfWeek =  moment.unix(dateInUnix).format('dddd')
    return dayOfWeek
}

function addBreakNode(nodeToAddTo){
    let breakNode = document.createElement('br');
    nodeToAddTo.appendChild(breakNode);
}

function addWeatherIconToDOM(dayNumber, iconDescription){
    let dayID = 'day '+String(dayNumber);
    let dayNode = document.getElementById(dayID)
    let canvasNode = document.createElement('CANVAS');
    let skycons = new Skycons({"color": "white"});
    skycons.add(canvasNode, iconDescription)
    dayNode.appendChild(canvasNode);
    skycons.play();
    addBreakNode(dayNode);
}

function addTextNodeToDOM(dayNumber, text){
    let dayID = 'day '+String(dayNumber);
    let dayNode = document.getElementById(dayID)
    let newTextNode = document.createTextNode(text);
    dayNode.appendChild(newTextNode);
    addBreakNode(dayNode);
}

function addDailyWeatherResultsToDOM(dailyData){
    let i = 1;
    while (i <= 7){
        dayData = dailyData[i-1]
        dayOfWeek = getDayOfWeekFromJSON(dayData)
        dailyHigh = dayData.temperatureHigh;
        dailyLow = dayData.temperatureLow;
        weatherDescription = dayData.icon;
        addTextNodeToDOM(i, dayOfWeek)
        addWeatherIconToDOM(i, weatherDescription)
        addTextNodeToDOM(i, dailyHigh +' HI / ' + dailyLow + ' LO')
        i += 1
    }
}

function clearDOM(){
    let i = 1;
    while (i <= 7){
        let id = 'day '+String(i);
        let dayDiv = document.getElementById(id);
        dayDiv.innerHTML = "";
        i += 1;
    }
}

function getWeatherAndAddToDOM(){
    let location = getLocationFromPage();
    if (!checkIfValidGPSCoords(location)){
        window.alert('Please enter valid coordinates.')
    }
    else {
        clearDOM();
        getWeatherInfo(location)
            .then((json) => {
                console.log(json);
                dailyData = getDailyDataFromJSON(json);
                addDailyWeatherResultsToDOM(dailyData);
            })
            
    }  
}