package main

import (
    "log"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"os"
)

type Location struct {
	Location string
}

func getInfoFromJSONBody(w http.ResponseWriter, r *http.Request) (string){
	b, err := ioutil.ReadAll(r.Body)
	var location Location
	err = json.Unmarshal(b, &location)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
	return location.Location
}

func getAPIURL(location_coords string)(string){
	starting_url := "https://api.darksky.net/forecast/026d59d2af0e72969f9968eaf88b6354/"
	final_url := starting_url + location_coords
	return final_url
}

func getLocationFromAPI(location_coords string) (*http.Response){
	url := getAPIURL(location_coords)
	resp, _ := http.Get(url)
	return resp
}

func postRequestHandler(w http.ResponseWriter, r *http.Request){
	location_coords := getInfoFromJSONBody(w, r)
	location_response := getLocationFromAPI(location_coords)
	response_data, err := ioutil.ReadAll(location_response.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(response_data)
}


func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/submit", postRequestHandler)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
    log.Fatal(http.ListenAndServe(":"+port, nil))
}