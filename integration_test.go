package main

import (
	"testing"
)


func TestGetLocationFromAPI(t *testing.T){
	result := getLocationFromAPI("35,-130")
	if result.StatusCode != 200 {
		t.Error("expected 200 status code")
	}
}
